免费的编程中文书籍索引
============================

 [![](https://travis-ci.org/justjavac/free-programming-books-zh_CN.svg?branch=master)](https://gitee.com/Li-Ren )

免费的编程中文书籍索引，欢迎投稿。

## 参与交流

欢迎大家将珍藏已久的经典免费书籍共享出来，您可以：

* 使用 [Issues](https://gitee.com/Li-Ren/programming-books/issues ) 以及 Pull Request

## 目录

* [Android](#android)
* [APP](#app)
* [C/C++](#cc)
* [C#](#c)
* [CSS/HTML](#csshtml)
* [Erlang](#erlang)
* [Fortran](#fortran)
* [GIT](#GIT)
* [Go](#go)
* [iOS](#ios)
* [Java](#java)
* [JavaScript](#javascript)
* [Kotlin](#Kotlin)
* [Lua](#lua)
* [NoSQL](#NoSQL)
* [PHP](#php)
* [Python](#python)
* [R](#r)
* [Ruby](#ruby)
* [Rust](#rust)
* [Scala](#scala)
* [Shell](#shell)
* [Swift](#swift)
* [Web](#web服务器)
* [正则表达式](#正则表达式)
* 

## Android

* [Android Design(中文版)](http://www.apkbus.com/design/index.html)
* Google Material Design 正體中文版 ([译本一](https://wcc723.gitbooks.io/google_design_translate/content/style-icons.html) [译本二](https://github.com/1sters/material_design_zh))
* [Material Design 中文版](http://wiki.jikexueyuan.com/project/material-design/)
* [Google Android官方培训课程中文版](http://hukai.me/android-training-course-in-chinese/index.html)
* [Android学习之路](http://www.stormzhang.com/android/2014/07/07/learn-android-from-rookie/)
* [Android开发技术前线(android-tech-frontier)](https://github.com/bboyfeiyu/android-tech-frontier)
* [Point-of-Android](https://github.com/FX-Max/Point-of-Android) Android 一些重要知识点解析整理
* [Android6.0新特性详解](http://leanote.com/blog/post/561658f938f41126b2000298)

[返回目录](#目录)

## APP

* [Apache Cordova 开发指南](https://github.com/waylau/cordova-dev-guide)

[返回目录](#目录)

## C/C++

* [C/C++ 中文参考手册](http://zh.cppreference.com/) (欢迎大家参与在线翻译和校对)
* [C 语言编程透视](https://www.gitbook.com/book/tinylab/cbook/details)
* [C++ 并发编程指南](https://github.com/forhappy/Cplusplus-Concurrency-In-Practice)
* [Linux C编程一站式学习](http://akaedu.github.io/book/) (宋劲杉, 北京亚嵌教育研究中心)
* [CGDB中文手册](https://github.com/leeyiw/cgdb-manual-in-chinese)
* [100个gdb小技巧](https://github.com/hellogcc/100-gdb-tips/blob/master/src/index.md)
* [100个gcc小技巧](https://github.com/hellogcc/100-gcc-tips/blob/master/src/index.md)
* [ZMQ 指南](https://github.com/anjuke/zguide-cn)
* [How to Think Like a Computer Scientist](http://www.ituring.com.cn/book/1203) (中英文版)
* [跟我一起写Makefile(PDF)](http://scc.qibebt.cas.cn/docs/linux/base/%B8%FA%CE%D2%D2%BB%C6%F0%D0%B4Makefile-%B3%C2%F0%A9.pdf)
* [GNU make中文手册](http://www.yayu.org/book/gnu_make/)
* [GNU make 指南](http://docs.huihoo.com/gnu/linux/gmake.html)
* [Google C++ 风格指南](http://zh-google-styleguide.readthedocs.org/en/latest/google-cpp-styleguide/contents/)
* [C/C++ Primer](https://github.com/andycai/cprimer) (by @andycai)
* [简单易懂的C魔法](http://www.nowamagic.net/librarys/books/contents/c)
* [C++ FAQ LITE(中文版)](http://www.sunistudio.com/cppfaq/)
* [C++ Primer 5th Answers](https://github.com/Mooophy/Cpp-Primer)
* [C++ 并发编程(基于C++11)](https://www.gitbook.com/book/chenxiaowei/cpp_concurrency_in_action/details)
* [QT 教程](http://www.kuqin.com/qtdocument/tutorial.html)
* [DevBean的《Qt学习之路2》(Qt5)](http://www.devbean.net/category/qt-study-road-2/)
* [C++ Template 进阶指南](https://github.com/wuye9036/CppTemplateTutorial)
* [libuv中文教程](https://github.com/luohaha/Chinese-uvbook)
* [Boost 库中文教程](http://zh.highscore.de/cpp/boost/)
* [笨办法学C](https://github.com/wizardforcel/lcthw-zh)
* [高速上手 C++11/14/17](https://github.com/changkun/modern-cpp-tutorial)

[返回目录](#目录)

## C&#35;

* [MSDN C# 中文文档](https://msdn.microsoft.com/zh-cn/library/kx37x362.aspx)
* [.NET 类库参考](https://msdn.microsoft.com/zh-cn/library/gg145045.aspx)
* [ASP.NET MVC 5 入门指南](http://www.cnblogs.com/powertoolsteam/p/aspnetmvc5-tutorials-grapecity.html)
* [超全面的 .NET GDI+ 图形图像编程教程](http://www.cnblogs.com/LonelyShadow/p/4162318.html)
* [.NET控件开发基础](https://github.com/JackWangCUMT/customcontrol)
* [.NET开发要点精讲（初稿）](https://github.com/sherlockchou86/-free-ebook-.NET-)

[返回目录](#目录)

##CSS/HTML

* [学习CSS布局](http://zh.learnlayout.com/)
* [通用 CSS 笔记、建议与指导](https://github.com/chadluo/CSS-Guidelines/blob/master/README.md)
* [CSS参考手册](http://css.doyoe.com/)
* [Emmet 文档](http://yanxyz.github.io/emmet-docs/)
* [前端代码规范](http://alloyteam.github.io/CodeGuide/) (腾讯 AlloyTeam 团队)
* [HTML和CSS编码规范](http://codeguide.bootcss.com/)
* [Sass Guidelines 中文](http://sass-guidelin.es/zh/)
* [CSS3 Tutorial 《CSS3 教程》](https://github.com/waylau/css3-tutorial)
* [MDN HTML 中文文档](https://developer.mozilla.org/zh-CN/docs/Web/HTML)
* [MDN CSS 中文文档](https://developer.mozilla.org/zh-CN/docs/Web/CSS)

[返回目录](#目录)

## Erlang

* [21天学通Erlang](http://xn--21erlang-p00o82pmp3o.github.io/)

[返回目录](#目录)

## Fortran

* [Fortran77和90/95编程入门](http://micro.ustc.edu.cn/Fortran/ZJDing/)

[返回目录](#目录)

## GIT

- [Git - 简易指南](http://rogerdudler.github.io/git-guide/index.zh.html)
- [Git Community Book 中文版](http://gitbook.liuhui998.com)
- [git-flow 备忘清单](http://danielkummer.github.io/git-flow-cheatsheet/index.zh_CN.html)
- [Git 参考手册](http://gitref.justjavac.com)
- [Github帮助文档](https://github.com/waylau/github-help)
- [GitHub秘籍](https://snowdream86.gitbooks.io/github-cheat-sheet/content/zh/)
- [Git教程](http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000) - 廖雪峰

[返回目录](#目录)

## Go

* [Go编程基础](https://github.com/Unknwon/go-fundamental-programming)
* [Go入门指南](https://github.com/Unknwon/the-way-to-go_ZH_CN)
* [学习Go语言](http://mikespook.com/learning-go/)
* [Go Web 编程](https://github.com/astaxie/build-web-application-with-golang) (此书已经出版，希望开发者们去购买，支持作者的创作)
* [Go实战开发](https://github.com/astaxie/Go-in-Action) (当我收录此项目时，作者已经写完第三章，如果读完前面章节觉得有帮助，可以给作者[捐赠](https://me.alipay.com/astaxie)，以鼓励作者的继续创作)
* [Network programming with Go 中文翻译版本](https://github.com/astaxie/NPWG_zh)
* [Effective Go](http://www.hellogcc.org/effective_go.html)
* [Go 语言标准库](https://github.com/polaris1119/The-Golang-Standard-Library-by-Example)
* [Golang标准库文档](http://godoc.ml/)
* [Revel 框架手册](http://gorevel.cn/docs/manual/index.html)
* [Java程序员的Golang入门指南](http://blog.csdn.net/dc_726/article/details/46565241)
* [Go命令教程](https://github.com/hyper-carrot/go_command_tutorial)
* [Go语言博客实践](https://github.com/achun/Go-Blog-In-Action)
* [Go 官方文档翻译](https://github.com/golang-china/golangdoc.translations)
* [深入解析Go](https://github.com/tiancaiamao/go-internals)
* [Go语言圣经(中文版)](https://bitbucket.org/golang-china/gopl-zh/wiki/Home) ([GitBook](https://www.gitbook.com/book/wizardforcel/gopl-zh/details))

[返回目录](#目录)

## iOS

* [iOS开发60分钟入门](https://github.com/qinjx/30min_guides/blob/master/ios.md)
* [iOS7人机界面指南](http://isux.tencent.com/ios-human-interface-guidelines-ui-design-basics-ios7.html)
* [Google Objective-C Style Guide 中文版](http://zh-google-styleguide.readthedocs.org/en/latest/google-objc-styleguide/)
* [iPhone 6 屏幕揭秘](http://wileam.com/iphone-6-screen-cn/)
* [Apple Watch开发初探](http://nilsun.github.io/apple-watch/)
* [马上着手开发 iOS 应用程序](https://developer.apple.com/library/ios/referencelibrary/GettingStarted/RoadMapiOSCh/index.html)
* [网易斯坦福大学公开课：iOS 7应用开发字幕文件](https://github.com/jkyin/Subtitle)

[返回目录](#目录)

## Java

* [Activiti 5.x 用户指南](https://github.com/waylau/activiti-5.x-user-guide)
* [Apache MINA 2 用户指南](https://github.com/waylau/apache-mina-2.x-user-guide)
* [Apache Shiro 用户指南](https://github.com/waylau/apache-shiro-1.2.x-reference)
* [Google Java编程风格指南](http://www.hawstein.com/posts/google-java-style.html)
* [H2 Database 教程](https://github.com/waylau/h2-database-doc)
* [Java Servlet 3.1 规范](https://github.com/waylau/servlet-3.1-specification)
* [Java 编码规范](https://github.com/waylau/java-code-conventions)
* [Java 编程思想](https://java.quanke.name) - quanke
* [Jersey 2.x 用户指南](https://github.com/waylau/Jersey-2.x-User-Guide)
* [JSSE 参考指南](https://github.com/waylau/jsse-reference-guide)
* [MyBatis中文文档](http://mybatis.github.io/mybatis-3/zh/index.html)
* [Netty 4.x 用户指南](https://github.com/waylau/netty-4-user-guide)
* [Netty 实战(精髓)](https://github.com/waylau/essential-netty-in-action)
* [Nutz-book Nutz烹调向导](http://nutzbook.wendal.net)
* [Nutz文档](https://nutzam.com/core/nutz_preface.html)
* [REST 实战](https://github.com/waylau/rest-in-action)
* [Spring Boot参考指南](https://github.com/qibaoguang/Spring-Boot-Reference-Guide) (🚧 *翻译中*)
* [Spring Framework 4.x参考文档](https://github.com/waylau/spring-framework-4-reference)
* [用jersey构建REST服务](https://github.com/waylau/RestDemo)

[返回目录](#目录)

## JavaScript

* [现代 Javascript 教程](https://zh.javascript.info/)
* [Google JavaScript 代码风格指南](http://bq69.com/blog/articles/script/868/google-javascript-style-guide.html)
* [Google JSON 风格指南](https://github.com/darcyliu/google-styleguide/blob/master/JSONStyleGuide.md)
* [Airbnb JavaScript 规范](https://github.com/adamlu/javascript-style-guide)
* [JavaScript 标准参考教程（alpha）](http://javascript.ruanyifeng.com/)
* [Javascript编程指南](http://pij.robinqu.me/) ([源码](https://github.com/RobinQu/Programing-In-Javascript))
* [javascript 的 12 个怪癖](https://github.com/justjavac/12-javascript-quirks)
* [JavaScript 秘密花园](http://bonsaiden.github.io/JavaScript-Garden/zh/)
* [JavaScript核心概念及实践](http://icodeit.org/jsccp/) (PDF) (此书已由人民邮电出版社出版发行，但作者依然免费提供PDF版本，希望开发者们去购买，支持作者)
* [《JavaScript 模式》](https://github.com/jayli/javascript-patterns) “JavaScript patterns”中译本
* [命名函数表达式探秘](http://justjavac.com/named-function-expressions-demystified.html)  (注:原文由[为之漫笔](http://www.cn-cuckoo.com)翻译，原始地址无法打开，所以此处地址为我博客上的备份)
* [学用 JavaScript 设计模式](http://www.oschina.net/translate/learning-javascript-design-patterns) (开源中国)
* [深入理解JavaScript系列](http://www.cnblogs.com/TomXu/archive/2011/12/15/2288411.html)
* [ECMAScript 5.1 中文版](http://yanhaijing.com/es5)
* [ECMAScript 6 入门](http://es6.ruanyifeng.com/) (作者：阮一峰)
* [JavaScript Promise迷你书](http://liubin.github.io/promises-book/)
* [You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS) (深入JavaScript语言核心机制的系列图书)
* [JavaScript 教程](http://www.liaoxuefeng.com/wiki/001434446689867b27157e896e74d51a89c25cc8b43bdb3000) 廖雪峰
* [MDN JavaScript 中文文档](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript)
* jQuery
    * [jQuery 解构](http://www.cn-cuckoo.com/deconstructed/jquery.html)
    * [简单易懂的JQuery魔法](http://www.nowamagic.net/librarys/books/contents/jquery)
    * [How to write jQuery plugin](http://i5ting.github.io/How-to-write-jQuery-plugin/build/jquery.plugin.html)
    * [You Don't Need jQuery](https://github.com/oneuijs/You-Dont-Need-jQuery/blob/master/README.zh-CN.md)
    * [如何实现一个类jQuery？](https://github.com/MeCKodo/forchange)
* Node.js
    * [Node入门](http://www.nodebeginner.org/index-zh-cn.html)
    * [七天学会NodeJS](http://nqdeng.github.io/7-days-nodejs/)
    * [Nodejs Wiki Book](https://github.com/nodejs-tw/nodejs-wiki-book) (繁体中文)
    * [express.js 中文文档](http://expressjs.jser.us/)
    * [koa 中文文档](https://github.com/guo-yu/koa-guide)
    * [一起学koa](http://base-n.github.io/koa-generator-examples/)
    * [使用 Express + MongoDB 搭建多人博客](https://github.com/nswbmw/N-blog)
    * [Express框架](http://javascript.ruanyifeng.com/nodejs/express.html)
    * [Node.js 包教不包会](https://github.com/alsotang/node-lessons)
    * [Learn You The Node.js For Much Win! (中文版)](https://www.npmjs.com/package/learnyounode-zh-cn)
    * [Node debug 三法三例](http://i5ting.github.io/node-debug-tutorial/)
    * [nodejs中文文档](https://www.gitbook.com/book/0532/nodejs/details)
    * [orm2 中文文档](https://github.com/wizardforcel/orm2-doc-zh-cn)
    * [一起学 Node.js](https://github.com/nswbmw/N-blog)
* underscore.js
    * [Underscore.js中文文档](http://learningcn.com/underscore/)
* backbone.js
    * [backbone.js中文文档](http://www.css88.com/doc/backbone/)
    * [backbone.js入门教程](http://www.the5fire.com/backbone-js-tutorials-pdf-download.html) (PDF)
    * [Backbone.js入门教程第二版](https://github.com/the5fire/backbonejs-learning-note)
    * [Developing Backbone.js Applications(中文版)](http://feliving.github.io/developing-backbone-applications/)
* AngularJS
    * [AngularJS最佳实践和风格指南](https://github.com/mgechev/angularjs-style-guide/blob/master/README-zh-cn.md)
    * [AngularJS中译本](https://github.com/peiransun/angularjs-cn)
    * [AngularJS入门教程](https://github.com/zensh/AngularjsTutorial_cn)
    * [构建自己的AngularJS](https://github.com/xufei/Make-Your-Own-AngularJS/blob/master/01.md)
    * [在Windows环境下用Yeoman构建AngularJS项目](http://www.waylau.com/build-angularjs-app-with-yeoman-in-windows/)
* Zepto.js
    * [Zepto.js 中文文档](http://mweb.baidu.com/zeptoapi/)
* Sea.js
    * [Hello Sea.js](http://island205.com/HelloSea.js/)
* React.js
    * [React 学习之道](https://github.com/the-road-to-learn-react/the-road-to-learn-react-chinese)
    * [React.js 小书](https://github.com/huzidaha/react-naive-book)
    * [React.js 中文文档](https://doc.react-china.org/)
    * [React webpack-cookbook](https://github.com/fakefish/react-webpack-cookbook)
    * [React 入门教程](http://fraserxu.me/intro-to-react/)
    * [React Native 中文文档(含最新Android内容)](http://wiki.jikexueyuan.com/project/react-native/)
    * [Learn React & Webpack by building the Hacker News front page](https://github.com/theJian/build-a-hn-front-page)
* impress.js
    * [impress.js的中文教程](https://github.com/kokdemo/impress.js-tutorial-in-Chinese)
* CoffeeScript
    * [CoffeeScript Cookbook](http://island205.com/coffeescript-cookbook.github.com/)
    * [The Little Book on CoffeeScript中文版](http://island205.com/tlboc/)
    * [CoffeeScript 编码风格指南](https://github.com/geekplux/coffeescript-style-guide)
* TypeScipt
    * [TypeScript Handbook](https://zhongsp.gitbooks.io/typescript-handbook/content/)
* ExtJS
    * [Ext4.1.0 中文文档](http://extjs-doc-cn.github.io/ext4api/)
* Meteor
    * [Discover Meteor](http://zh.discovermeteor.com/)
    * [Meteor 中文文档](http://docs.meteorhub.org/#/basic/)
    * [Angular-Meteor 中文教程](http://angular.meteorhub.org/)
* [Chrome扩展及应用开发](http://www.ituring.com.cn/minibook/950)

[返回目录](#目录)

## Kotlin

* [developing-android-apps-with-kotlin](https://www.udacity.com/course/developing-android-apps-with-kotlin--ud9012)

[返回目录](#目录)

## Lua

* [Lua编程入门](https://github.com/andycai/luaprimer)
* [Lua 5.1 参考手册 中文翻译](http://www.codingnow.com/2000/download/lua_manual.html)
* [Lua 5.3 参考手册 中文翻译](http://cloudwu.github.io/lua53doc/)
* [Lua源码欣赏](http://www.codingnow.com/temp/readinglua.pdf)

[返回目录](#目录)

##NoSQL

- [Disque 使用教程](http://disque.huangz.me)
- [Redis 命令参考](http://redisdoc.com)
- [Redis 设计与实现](http://redisbook.com)
- [The Little MongoDB Book](https://github.com/justinyhuang/the-little-mongodb-book-cn/blob/master/mongodb.md)
- [The Little Redis Book](https://github.com/JasonLai256/the-little-redis-book/blob/master/cn/redis.md)
- [带有详细注释的 Redis 2.6 代码](https://github.com/huangz1990/annotated_redis_source)
- [带有详细注释的 Redis 3.0 代码](https://github.com/huangz1990/redis-3.0-annotated)

[返回目录](#目录)

## PHP

* [PHP 官方手册](http://php.net/manual/zh/)
* [PHP调试技术手册](http://www.laruence.com/2010/06/21/1608.html)(PDF)
* PHP之道：php-the-right-way ([@wulijun版](http://wulijun.github.io/php-the-right-way/) [PHPHub版](http://laravel-china.github.io/php-the-right-way/))
* [PHP 最佳实践](https://github.com/justjavac/PHP-Best-Practices-zh_CN)
* [PHP 开发者实践](https://ryancao.gitbooks.io/php-developer-prepares/content/)
* [深入理解PHP内核](https://github.com/reeze/tipi)
* [PHP扩展开发及内核应用](http://www.walu.cc/phpbook/)
* [Laravel5.1 中文文档](http://laravel-china.org/docs/5.1)
* [Laravel 5.1 LTS 速查表](https://cs.phphub.org/)
* [Symfony2 Cookbook 中文版](http://wiki.jikexueyuan.com/project/symfony-cookbook/)(版本 2.7.0 LTS)
* [Symfony2中文文档](http://symfony-docs-chs.readthedocs.org/en/latest/) (未译完)
* [YiiBook几本Yii框架的在线教程](http://yiibook.com//doc)
* [深入理解 Yii 2.0](http://www.digpage.com/)
* [Yii 框架中文文檔](http://www.yiichina.com/)
* [简单易懂的PHP魔法](http://www.nowamagic.net/librarys/books/contents/php)
* [swoole文档及入门教程](https://github.com/LinkedDestiny/swoole-doc)
* [Composer 中文网](http://www.phpcomposer.com)
* [Slim 中文文档](http://ww1.minimee.org/php/slim)
* [Lumen 中文文档](http://lumen.laravel-china.org/)
* [PHPUnit 中文文档](https://phpunit.de/manual/current/zh_cn/installation.html)

[返回目录](#目录)

## Python

* [廖雪峰 Python 2.7 中文教程](http://www.liaoxuefeng.com/wiki/001374738125095c955c1e6d8bb493182103fac9270762a000)
* [廖雪峰 Python 3 中文教程](http://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
* [简明Python教程](http://www.kuqin.com/abyteofpython_cn/)
* [简明 Python 教程(Python 3)](https://legacy.gitbook.com/book/lenkimo/byte-of-python-chinese-edition/details)
* [零基础学 Python 第一版](http://www.kancloud.cn/kancloud/python-basic)
* [零基础学 Python 第二版](http://www.kancloud.cn/kancloud/starter-learning-python)
* [可爱的 Python](http://lovelypython.readthedocs.org/en/latest/)
* [Python 2.7 官方教程中文版](http://www.pythondoc.com/pythontutorial27/index.html)
* [Python 3.3 官方教程中文版](http://www.pythondoc.com/pythontutorial3/index.html)
* [Python Cookbook 中文版](http://www.kancloud.cn/thinkphp/python-cookbook)
* [Python3 Cookbook 中文版](https://github.com/yidao620c/python3-cookbook)
* [深入 Python](http://www.kuqin.com/docs/diveintopythonzh-cn-5.4b/html/toc/)
* [深入 Python 3](http://old.sebug.net/paper/books/dive-into-python3/)
* [PEP8 Python代码风格规范](https://code.google.com/p/zhong-wiki/wiki/PEP8)
* [Google Python 风格指南 中文版](http://zh-google-styleguide.readthedocs.org/en/latest/google-python-styleguide/)
* [Python入门教程](http://liam0205.me/2013/11/02/Python-tutorial-zh_cn/) ([PDF](http://liam0205.me/attachment/Python/The_Python_Tutorial_zh-cn.pdf))
* [笨办法学 Python](http://old.sebug.net/paper/books/LearnPythonTheHardWay/) ([PDF](http://liam0205.me/attachment/Python/PyHardWay/Learn_Python_The_Hard_Way_zh-cn.pdf) [EPUB](https://www.gitbook.com/download/epub/book/wizardforcel/lpthw))
* [Python自然语言处理中文版](http://pan.baidu.com/s/1qW4pvnY) （感谢陈涛同学的翻译，也谢谢 [@shwley](https://github.com/shwley) 联系了作者）
* [Python 绘图库 matplotlib 官方指南中文翻译](http://liam0205.me/2014/09/11/matplotlib-tutorial-zh-cn/)
* [Scrapy 0.25 文档](http://scrapy-chs.readthedocs.org/zh_CN/latest/)
* [ThinkPython](https://github.com/carfly/thinkpython-cn)
* [ThinkPython 2ed](https://github.com/bingjin/ThinkPython2-CN)
* [Python快速教程](http://www.cnblogs.com/vamei/archive/2012/09/13/2682778.html)
* [Python 正则表达式操作指南](http://wiki.ubuntu.org.cn/Python正则表达式操作指南)
* [python初级教程：入门详解](http://www.crifan.com/files/doc/docbook/python_beginner_tutorial/release/html/python_beginner_tutorial.html)
* [Twisted 与异步编程入门](https://www.gitbook.com/book/likebeta/twisted-intro-cn/details)
* [TextGrocery 中文 API](http://textgrocery.readthedocs.org/zh/latest/index.html) ( 基于svm算法的一个短文本分类 Python 库 )
* [Requests: HTTP for Humans](http://requests-docs-cn.readthedocs.org/zh_CN/latest/)
* [Pillow 中文文档](http://pillow-cn.readthedocs.org/en/latest/#)
* [PyMOTW 中文版](http://pymotwcn.readthedocs.org/en/latest/index.html)
* [Python 官方文档中文版](http://data.digitser.net/zh-CN/python_index.html)
* [Fabric 中文文档](http://fabric-chs.readthedocs.org)
* [Beautiful Soup 4.2.0 中文文档](http://beautifulsoup.readthedocs.org/zh_CN/latest/)
* [Python 中的 Socket 编程](https://legacy.gitbook.com/book/keelii/socket-programming-in-python-cn/details)
* [用Python做科学计算](http://old.sebug.net/paper/books/scipydoc)
* [Sphinx 中文文档](http://www.pythondoc.com/sphinx/index.html)
* [精通 Python 设计模式](https://github.com/cundi/Mastering.Python.Design.Patterns)
* [python 安全编程教程](https://github.com/smartFlash/pySecurity)
* [程序设计思想与方法](https://www.gitbook.com/book/wizardforcel/sjtu-cs902-courseware/details)
* [知乎周刊·编程小白学Python](https://read.douban.com/ebook/16691849/)
* [Scipy 讲义](https://github.com/cloga/scipy-lecture-notes_cn)
* [Python 学习笔记 基础篇](http://www.kuqin.com/docs/pythonbasic.html)
* [Python 学习笔记 模块篇](http://www.kuqin.com/docs/pythonmodule.html)
* [Python 标准库 中文版](http://old.sebug.net/paper/books/python/%E3%80%8APython%E6%A0%87%E5%87%86%E5%BA%93%E3%80%8B%E4%B8%AD%E6%96%87%E7%89%88.pdf)
* [Python进阶](https://www.gitbook.com/book/eastlakeside/interpy-zh/details)
* [Python 核心编程 第二版](https://wizardforcel.gitbooks.io/core-python-2e/content/) CPyUG译
* [Python最佳实践指南](http://pythonguidecn.readthedocs.io/zh/latest/)
* [Python 精要教程](https://www.gitbook.com/book/wizardforcel/python-essential-tutorial/details)
* [Python 量化交易教程](https://www.gitbook.com/book/wizardforcel/python-quant-uqer/details)
* Django
    * [Django 1.5 文档中文版](http://django-chinese-docs.readthedocs.org/en/latest/) 正在翻译中
    * [Django 2.0 文档中文版](https://docs.djangoproject.com/zh-hans/2.0/)
    * [Django 最佳实践](https://github.com/yangyubo/zh-django-best-practices)
    * [Django 2.1 搭建个人博客教程](https://www.dusaiphoto.com/article/detail/2/) ( 编写中 )
    * [Django搭建简易博客教程](https://www.gitbook.com/book/andrew-liu/django-blog/details)
    * [The Django Book 中文版](http://djangobook.py3k.cn/2.0/)
    * [Django 设计模式与最佳实践](https://github.com/cundi/Django-Design-Patterns-and-Best-Practices)
    * [Django 网站开发 Cookbook](https://github.com/cundi/Web.Development.with.Django.Cookbook)
    * [Django Girls 學習指南](https://www.gitbook.com/book/djangogirlstaipei/django-girls-taipei-tutorial/details)
* Flask
    * [Flask 文档中文版](http://docs.jinkan.org/docs/flask/)
    * [Jinja2 文档中文版](http://docs.jinkan.org/docs/jinja2/)
    * [Werkzeug 文档中文版](http://werkzeug-docs-cn.readthedocs.org/zh_CN/latest/)
    * [Flask之旅](http://spacewander.github.io/explore-flask-zh/)
    * [Flask 扩展文档汇总](https://www.gitbook.com/book/wizardforcel/flask-extension-docs/details)
    * [Flask 大型教程](http://www.pythondoc.com/flask-mega-tutorial/index.html)
    * [SQLAlchemy 中文文档](http://docs.jinkan.org/docs/flask-sqlalchemy/)
* web.py
    * [web.py 0.3 新手指南](http://webpy.org/tutorial3.zh-cn)
    * [Web.py Cookbook 简体中文版](http://webpy.org/cookbook/index.zh-cn)
* Tornado
    * [Introduction to Tornado 中文翻译](http://demo.pythoner.com/itt2zh/index.html)
    * [Tornado源码解析](http://www.nowamagic.net/academy/detail/13321002)
    * [Tornado 4.3 文档中文版](https://tornado-zh.readthedocs.org/zh/latest/)

[返回目录](#目录)

## R

* [R语言忍者秘笈](https://github.com/yihui/r-ninja)

[返回目录](#目录)

## Ruby

* [Ruby 风格指南](https://github.com/JuanitoFatas/ruby-style-guide/blob/master/README-zhCN.md)
* [Rails 风格指南](https://github.com/JuanitoFatas/rails-style-guide/blob/master/README-zhCN.md)
* [笨方法學 Ruby](http://lrthw.github.io/)
* [Ruby on Rails 指南](http://guides.ruby-china.org/)
* [Ruby on Rails 實戰聖經](https://ihower.tw/rails4/index.html)
* [Ruby on Rails Tutorial 原书第 3 版](http://railstutorial-china.org/) (本书网页版免费提供，电子版以 PDF、EPub 和 Mobi 格式提供购买，仅售 9.9 美元)
* [Rails 实践](http://rails-practice.com/content/index.html)
* [Rails 5 开发进阶(Beta)](https://www.gitbook.com/book/kelby/rails-beginner-s-guide/details)
* [Rails 102](https://www.gitbook.com/book/rocodev/rails-102/details)
* [编写Ruby的C拓展](https://wusuopu.gitbooks.io/write-ruby-extension-with-c/content/)
* [Ruby 源码解读](https://ruby-china.org/topics/22386)
* [Ruby中的元编程](http://deathking.github.io/metaprogramming-in-ruby/)

[返回目录](#目录)

## Rust

* [Rust编程语言 中文翻译](https://kaisery.github.io/trpl-zh-cn/)
* [Rust Primer](https://github.com/rustcc/RustPrimer)

[返回目录](#目录)

## Scala

* [Scala课堂](http://twitter.github.io/scala_school/zh_cn/index.html) (Twitter的Scala中文教程)
* [Effective Scala](http://twitter.github.io/effectivescala/index-cn.html)(Twitter的Scala最佳实践的中文翻译)
* [Scala指南](http://zh.scala-tour.com/)

[返回目录](#目录)

## Shell

* [Shell脚本编程30分钟入门](https://github.com/qinjx/30min_guides/blob/master/shell.md)
* [Bash脚本15分钟进阶教程](http://blog.sae.sina.com.cn/archives/3606)
* [Linux工具快速教程](https://github.com/me115/linuxtools_rst)
* [shell十三问](https://github.com/wzb56/13_questions_of_shell)
* [Shell编程范例](https://www.gitbook.com/book/tinylab/shellbook/details)

[返回目录](#目录)

## Swift

* [The Swift Programming Language 中文版](http://numbbbbb.github.io/the-swift-programming-language-in-chinese/)
* [Swift 语言指南](http://dev.swiftguide.cn)
* [Stanford 公开课，Developing iOS 8 Apps with Swift 字幕翻译文件](https://github.com/x140yu/Developing_iOS_8_Apps_With_Swift)
* [C4iOS - COSMOS](http://c4ios.swift.gg)

[返回目录](#目录)

##WEB服务器

* [Apache 中文手册](http://works.jinbuguo.com/apache/menu22/index.html)

* [Nginx开发从入门到精通](http://tengine.taobao.org/book/index.html) - 淘宝团队

* [Nginx教程从入门到精通](http://www.ttlsa.com/nginx/nginx-stu-pdf/) - 运维生存时间 (PDF)

[返回目录](#目录)

##正则表达式

- [正则表达式-菜鸟教程](http://www.runoob.com/regexp/regexp-tutorial.html)
- [正则表达式30分钟入门教程](https://web.archive.org/web/20161119141236/http://deerchao.net:80/tutorials/regex/regex.htm)